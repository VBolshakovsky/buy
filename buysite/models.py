from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.urls import reverse
from pytils.translit import slugify

#Статус покупки
STATUS = (
    ('Buy', 'Куплено'),
    ('Planned', 'Запланировано'),
)

#Признак расчета
OPERTYPE = (
    (1, 'Приход'),
    (2, 'Возврат прихода'),
    (3, 'Расход'),
    (4, 'Возврат расхода'),
)

#Применяемая система налогообложения
    #«1» — общая система налогообложения;
    #«2» — упрощенная система налогообложения (доход);
    #«4» — упрощенная система налогообложения (доход минус расход);
    #«8» — единый налог на вмененный доход;
    #«16» — единый сельскохозяйственный налог;
    #«32» — патентная система налогообложения. 
TAXTAPE = (
    (1, 'ОСН'),
    (2, 'УСН доход'),
    (4, 'УСН доход - Расход'),
    (8, 'ЕНВД'),
    (16, 'ЕСХН'),    
    (32, 'Патент'),    
)

#Ставка НДС:
NDSRATE = (
    ('1', 'НДС 20%'),
    ('2', 'НДС 10%'),
    ('3', 'НДС 20/120'),
    ('4', 'НДС 10/110'),
    ('5', 'НДС 0%'),    
    ('6', 'НДС не облагается'),    
)

class Сategory(models.Model):
    #Справочник/Группы продуктов
    name = models.CharField(max_length=20, unique=True, verbose_name='Категория')
    slug = models.SlugField(unique=True, verbose_name='slug')
    comment = models.CharField(max_length=250, blank=True, verbose_name='Комментарий')
    
    def get_absolute_url(self):
        return reverse('category', kwargs={'category_slug': self.slug})
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Сategory, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name='Категория товаров'
        verbose_name_plural = 'Категория товаров'

class Measure(models.Model):
    #Справочник/Единица измерения
    name = models.CharField(max_length=100, unique=True, verbose_name='Единица измерения')  
    name_mini = models.CharField(max_length=100, unique=True, verbose_name='Единица измерения(сокр.)')  
    slug = models.CharField(max_length=20, unique=True, verbose_name='slug')
    
    def __str__(self):
        return self.name_mini
    
    class Meta:
        verbose_name='Единица измерения'
        verbose_name_plural = 'Единица измерения'
        

class Group(models.Model):
    #Продукт
        #name               Название товарной позиции
        #slug               Slug
        #category           Группа товаров(class Сategory)
        #measure            Единица измерения
        #expiration_date    Срок годности
        #comment            Комментарий
    
    name = models.CharField(max_length=250, verbose_name='Наименование')
    slug = models.SlugField(unique=True, verbose_name='slug')
    category = models.ForeignKey(Сategory, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Категория')
    measure = models.ForeignKey(Measure, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Единица измерения')
    comment = models.CharField(max_length=250, blank=True, verbose_name='Комментарий')
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('group', kwargs={'group_slug': self.slug})
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Group, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name='Статус'
        verbose_name_plural = 'Статус'
    
class Ticket (models.Model):
    #Чек
        #json           json от ФНС
        #qr             qr Code
        #operationType  Признак расчета    
        #datetime       Дата/Время
        #datetimeunix   Дата(unix)
        #totalsum       Общая сумма
        #cashTotalSum   Сумма, уплаченная наличными в рублях.
        #ecashTotalSum  Сумма, уплаченная безналичными, в рублях.
        #taxationType   Система налогообложения
        #internetSign   Признак расчетов в Интернете
        #fd             Порядковый номер фискального документа(fiscaldocumentnumber)
        #fn             Номер фискального накопителя(fiscaldrivenumber)
        #fp             Фискальный признак документа(fiscalsign)
        #kkt            Регистрационнай номер кассового аппарата(kktregid)
        #requestNumber  Номер чека за смену
        #operator       Кассир
        #shiftnumber    Номер смены
        #retailplace    Место расчетов
        #seller_name    Наименование продавца
        #seller_inn     ИНН продавца
    
    json = models.JSONField(blank=True, null=True, verbose_name='json от ФНС')
    qr = models.CharField(max_length=300, blank=True, null=True, verbose_name='qr')
    operationType = models.PositiveIntegerField(choices=OPERTYPE, verbose_name='Признак расчета')
    datetime = models.DateTimeField(verbose_name='Дата')
    totalsum = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Сумма')
    cashTotalSum = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, verbose_name='Наличными')
    ecashTotalSum = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, verbose_name='БезНаличными')
    taxationType = models.PositiveIntegerField(blank=True, null=True, choices=TAXTAPE, verbose_name='Вид налогообложения')
    internetSign = models.BooleanField(blank=True, null=True, default=False, verbose_name='Через Интернет')
    fd = models.PositiveBigIntegerField(verbose_name='ФД')
    fn = models.PositiveBigIntegerField(verbose_name='ФН')
    fp = models.PositiveBigIntegerField(verbose_name='ФП')
    kkt = models.PositiveBigIntegerField(verbose_name='ККТ')
    requestNumber = models.PositiveBigIntegerField(verbose_name='Номер чека')
    operator = models.CharField(max_length=100, blank=True, null=True, verbose_name='Кассир')
    shiftnumber = models.CharField(max_length=30,blank=True, null=True, verbose_name='Номер смены')
    retailplace = models.CharField(max_length=300, blank=True, null=True, verbose_name='Место расчетов')
    user = models.CharField(max_length=300, blank=True, null=True, verbose_name='Наименование продавца')
    userInn = models.CharField(max_length=12,blank=True, null=True, verbose_name='ИНН продавца')

    def __str__(self):
        return str(self.fp)
    
    def get_absolute_url(self):
        return reverse('ticket', kwargs={'fp': self.fp})
    
    def datestr(self):
        return str(self.datetime.strftime('%d.%m.%Y'))

class Seller(models.Model):
    #Продавец
        #name       Наименование
        #slug       Slug
        #inn        ИНН
        #place      Где расположен
        #comment    Комментарий
    
    name = models.CharField(max_length=250, unique=True, verbose_name='Продавец')
    slug = models.CharField(max_length=20, unique=True, verbose_name='slug')
    inn = models.PositiveBigIntegerField(blank=True,verbose_name='ИНН')
    place = models.CharField(max_length=300, blank=True, verbose_name='Адрес')
    comment = models.CharField(max_length=250, blank=True, verbose_name='Комментарий')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name='Продавец'
        verbose_name_plural = 'Продавец'

class Buy(models.Model):
    #Покупка товара
        #name            Наименовение
        #state           Состояние(BuyState)
        #dateTime        Дата/время покупки
        #price           Цена
        #quantity        Колличество
        #measure         Единица измерения
        #sum             Сумма
        #group           Группа товаров(class Group)
        #user            Покупатель(class user)
        #seller          Продавец(class Seller)
        #ticket          Чек(class Ticket)
        #nds             Ставка НДС
        #ndsSum          Сумма НДС
        #productType     Признак предмета расчета
        #paymentType     Признак способа расчета
        #comment         Комментарий
        
    name = models.CharField(max_length=300, verbose_name='Наименование')
    state = models.CharField(max_length=32, choices=STATUS, default='Buy', verbose_name='Status')
    dateTime = models.DateTimeField(verbose_name='Дата/время')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    quantity = models.DecimalField(max_digits=6, decimal_places=3, verbose_name='Количество')
    measure = models.ForeignKey(Measure, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Единица измерения')
    sum = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Сумма')
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Группа')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Покупатель')
    seller = models.ForeignKey(Seller, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Продавец')
    ticket = models.ForeignKey(Ticket, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Чек')
    nds = models.CharField(choices=NDSRATE,max_length = 2, null=True, blank=True, verbose_name='Ставка НДС')
    ndsSum = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name='Сумма НДС')
    productType = models.CharField(max_length=10,null=True, blank=True, verbose_name='Предмета расчета')
    paymentType = models.CharField(max_length=10,null=True, blank=True, verbose_name='Способа расчета')
    comment = models.CharField(max_length=250, null=True, blank=True, verbose_name='Комментарий')
    
    def __str__(self):
        return self.name
    
    def datestr(self):
        return str(self.dateTime.strftime('%d.%m.%Y'))
    
class Profile(models.Model):
    #Расширение свойст пользователя(связана с user)
    #user           user
    #slug           slug
    #tel            Телефон
    #session_id     id сесси после аунтификации(Для ФНС)
    #session_date   Дата последнего получения session_id(Для ФНС)
    #refresh_token  Токен для обновления сессии(Для ФНС)
    #device_os      Какое устройство(Для ФНС)
    #client_version Версия клиента(Для ФНС)
    #device_id      Номер устройства(Для ФНС)
    #user_agent     Версия приложения(Для ФНС)
    #client_secret  Секретный ключ(Для ФНС)
    #os             Операционка устройства(Для ФНС)

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tel = models.CharField(max_length=13, blank=True, unique=True, verbose_name='Телефон')
    session_id = models.CharField(max_length=61, blank=True, verbose_name='id_сессии')
    session_date = models.DateTimeField(null=True, blank=True, verbose_name='Дата сессии')
    refresh_token = models.CharField(max_length=36, blank=True, verbose_name='refresh_token')
    device_os = models.CharField(max_length=20, blank=True, verbose_name='device_os')
    client_version = models.CharField(max_length=20, blank=True, verbose_name='client_version')
    device_id = models.CharField(max_length=40, blank=True, verbose_name='device_id')
    user_agent = models.CharField(max_length=150, blank=True, verbose_name='user_agent')
    client_secret = models.CharField(max_length=35, blank=True, verbose_name='client_secret')
    os = models.CharField(max_length=20, blank=True, verbose_name='os')
    
    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)