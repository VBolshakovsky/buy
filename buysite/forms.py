from datetime import datetime, date, time, timedelta
from pyexpat import model
from django import forms
from django.contrib.auth.models import User
from .models import Profile, Buy, Group

class AddQRDetailForm(forms.Form):
    #Форма для проверки чека по реквизитам
    fn = forms.IntegerField(required=True, label='ФН', initial=9960440301303377, widget=forms.NumberInput(attrs={'class':'fn', 'placeholder':'Номер фискального накопителя', 'autocomplete':'off', 'maxlength': '16'}))
    fd = forms.IntegerField(required=True, label='ФД', initial=21631, widget=forms.NumberInput(attrs={'class':'fd', 'placeholder':'Номер фискального документа', 'autocomplete':'off', 'maxlength': '16'}))
    fp = forms.IntegerField(required=True, label='ФП', initial=2641149654, widget=forms.NumberInput(attrs={'class':'fp', 'placeholder':'Фискальный признак документа', 'autocomplete':'off', 'maxlength': '16'}))
    sum = forms.DecimalField(max_digits=16, decimal_places=2, required=True, label='ИТОГО', initial=1303.13, widget=forms.NumberInput(attrs={'class':'sum', 'placeholder':'Сумма чека', 'autocomplete':'off', 'maxlength': '16'}))
    date = forms.DateField(required=True, label='ДАТА', initial=date(2022, 4, 1), widget=forms.TextInput(attrs={'type':'date'}))
    time = forms.TimeField(required=True, label='ВРЕМЯ', initial=time(18, 45), widget=forms.TextInput(attrs={'type':'time'}))
    type = forms.ChoiceField(required=True, label='Признак расчета', choices=((1, "Приход"), (2, "Возврат прихода"), (3, "Расход"), (4, "Возврат расхода")))
    tel = forms.CharField(required=True, label='ТЕЛЕФОН', initial='+7', widget=forms.NumberInput(attrs={'class':'tel', 'placeholder':'89008000101', 'autocomplete':'off', 'maxlength': '12'}))
    save = forms.BooleanField(required=False, label='Сохранить', initial=True)

class AddQRLineForm(forms.Form):
    #Форма для проверки чека по строке
    QRLine = forms.CharField(required=True, label='Строка из QR-кода', initial='t=20220522T122300&s=306.00&fn=9960440300624942&i=125520&fp=3008505265&n=1\n\
t=20220520T1425&s=1299.68&fn=9282440300755301&i=160137&fp=2243882731&n=1', widget=forms.Textarea(attrs={'placeholder':"Содержимое QR-кода", 'rows':"6", 'autocomplete':'off'}))
    tel = forms.CharField(required=True, label='ТЕЛЕФОН', initial='+7', widget=forms.NumberInput(attrs={'class':'tel', 'placeholder':'89008000101', 'autocomplete':'off', 'maxlength': '12'}))
    save = forms.BooleanField(required=False, label='Сохранить', initial=True)

class AddQRFileForm(forms.Form):
    #Форма для проверки чека по файлу
    file = forms.FileField(label = "Файл")
    save = forms.BooleanField(required=False, label='Сохранить', initial=True)
    
class SendSMSForm(forms.Form):
    #Форма для получения кода из смс
    sms = forms.IntegerField(required=True, label='Код из смс', widget=forms.NumberInput(attrs={'class':'sms', 'autofocus': 'autofocus', 'maxlength': '7'}))

class AddBuyForm(forms.ModelForm):
    #Форма для добавления покупок
    class Meta:
        model = Buy
        fields = ('name', 'state', 'dateTime', 'price', 'quantity', 'measure', 'group', 'comment', 'user')
        widgets = {
            'dateTime': forms.TextInput(attrs={'type':'datetime-local'}),
        }
        
class AddGroupForm(forms.ModelForm):
    #Форма для добавления групп товаров
    class Meta:
        model = Group
        fields = ('name', 'category', 'measure', 'comment')

class AddPersonalForm(forms.Form):
    #Форма фильтра личного кабинета
    dateFrom = forms.DateField(required=True, label='ДАТА', widget=forms.TextInput(attrs={'type':'date','width':'20px'}))
    dateBy = forms.DateField(required=True, label='-', widget=forms.TextInput(attrs={'type':'date'}))

class LoginForm(forms.Form):
    #Форма для авторизации
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class UserRegistrationForm(forms.ModelForm):
    #Форма для регистрации пользователей
    password = forms.CharField(label='Придумайте пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Пароль еще раз', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username',)

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Пароли не совпадают')
        return cd['password2']
    
class ProfileRegistrationForm(forms.ModelForm):
    #Форма для регистрации пользователей
    class Meta:
        model = Profile
        fields = ('tel',)


class UserEditForm(forms.ModelForm):
    #Форма для редактирования пользователя
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class ProfileEditForm(forms.ModelForm):
    #Форма для редактирования пользователя
    class Meta:
        model = Profile
        fields = ('tel','device_os','client_version','device_id','user_agent','client_secret','os')
        
class LoginForm(forms.Form):
    #Форма входа пользователя
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
        
    