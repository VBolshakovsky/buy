
import requests
import datetime
from django.contrib.auth.models import User
from ..models import Profile

class NalogAuth:
    
    ################################################
    #Класс для аунтификации в ФНС
    #input:  tel                exampl: +79998887755
    #output: self.sessionId     exampl: 61c44a4d6dee05355329c71f:a1c97e91-d562-4d55-873a-c150fca90c50
    #        self.refresh_token exampl: 9d13860d-56a6-4d49-8a85-0ac8792647c5
    ################################################
    HOST = 'irkkt-mobile.nalog.ru:8888'
    ACCEPT_LANGUAGE = 'ru-RU;q=1, en-US;q=0.9'
    ACCEPT = '*/*'
    
    def __init__(self,tel):
        
        self.session_id = None
        self.refresh_token = None
        self.errorlist =[]
        self.phone = tel
        self._get_options()
    
    def _get_options(self) -> None:
        #Функция для получения параметров, для авторизации в ФНС

        DEVICE_OS = 'iOS'
        CLIENT_VERSION = '2.9.0'
        USER_AGENT = 'billchecker/2.9.0 (iPhone; iOS 13.6; Scale/2.00)'
        CLIENT_SECRET = 'IyvrAbKt9h/8p6a7QPh8gpkXYQ4='
        DEVICE_ID = '7C82010F-16CC-446B-8F66-FC4080C11111'
        OS = 'Android'
        
        try:
            profile = Profile.objects.get(tel = self.phone)
        except:
            self._profile = None
            self._device_os = DEVICE_OS
            self._client_version = CLIENT_VERSION
            self._user_agent = USER_AGENT
            self._client_secret = CLIENT_SECRET
            self._device_id = DEVICE_ID
            self._os = OS
        else:
            self._profile = profile
            self._device_os = profile.device_os
            self._client_version = profile.client_version
            self._user_agent = profile.user_agent
            self._client_secret = profile.client_secret
            self._device_id = profile.device_id
            self._os = profile.os                        
            self.session_id = profile.session_id
            self.refresh_token = profile.refresh_token
            
        self.headers = {
            'Host': self.HOST,
            'Accept': self.ACCEPT,
            'Device-OS': self._device_os,
            'Device-Id': self._device_id,
            'clientVersion': self._client_version,
            'Accept-Language': self.ACCEPT_LANGUAGE,
            'User-Agent': self._user_agent,
        }
        
    def get_SMScode(self) -> None:
        #Функция для инициирования отправки смс кода от ФНС
        
        url = f'https://{self.HOST}/v2/auth/phone/request'
        payload = {
            'phone': self.phone,
            'client_secret': self._client_secret,
            'os': self._os
        }

        requests.post(url, json=payload, headers=self.headers)

    def _save_sessionId(self) -> None:
        #Функция для сохранения sessionId
        
        if not self.errorlist and self._profile != None:
            self._profile.session_id = self.session_id
            self._profile.refresh_token = self.refresh_token
            self._profile.session_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self._profile.save()
        
    def get_sessionId(self,SMScode: str):
        #Функция для получения sessionId и refresh_token от ФНС

        self._SMScode = SMScode

        url = f'https://{self.HOST}/v2/auth/phone/verify'
        payload = {
        'phone': self.phone,
        'client_secret': self._client_secret,
        'code': self._SMScode,
        "os": self._os
        }
        
        resp = requests.post(url, json=payload, headers=self.headers)
        try:
            self.session_id = resp.json()['sessionId']
            self.refresh_token = resp.json()['refresh_token']
        except:
            self.errorlist.append(f'Ответ сервера налогвой при получении sessionId: {resp},возможно не верно введен СМС код или просрочен.')
        
        if self._profile:
            self._save_sessionId()
        
    def refresh_session_id(self):
        #Функция для обновления sessionId и refresh_token от ФНС
        
        url = f'https://{self.HOST}/v2/mobile/users/refresh'
        payload = {
            'refresh_token': self.refresh_token,
            'client_secret': self._client_secret
        }

        resp = requests.post(url, json=payload, headers=self.headers)
        
        try:
            self.session_id = resp.json()['sessionId']
            self.refresh_token = resp.json()['refresh_token']
        except:
            self.errorlist.append(f'Ответ сервера налогвой при обнолении sessionId: {resp}')
        
        self._save_sessionId()

    def get_ticket_id(self, qr: str):
        #Функция для получения ticket_id от ФНС
        
        url = f'https://{self.HOST}/v2/ticket'
        payload = {'qr': qr}
        self.headers['sessionId'] = self.session_id
        
        resp = requests.post(url, json=payload, headers=self.headers)

        try:
            return resp.json()["id"]
        except:
            self.errorlist.append(f'Ответ сервера налогвой при получении ticket_id: {resp}')

    def get_ticket(self, qr: str) -> dict:
        #Функция для получения JSON от ФНС
        
        ticket_id = self.get_ticket_id(qr)
        
        url = f'https://{self.HOST}/v2/tickets/{ticket_id}'
        self.headers['sessionId'] = self.session_id
        self.headers['Content-Type'] = 'application/json'

        resp = requests.get(url, headers=self.headers)

        try:
            return resp.json()
        except:
            self.errorlist.append(f'Ответ сервера налогвой при получении чека JSON: {resp}')