import json
from ..services.functions import get_UNIXtoTime, get_qrText
from ..models import Ticket, Buy
from datetime import datetime, timedelta

class Parser:
    
    ################################################
    #Класс для аунтификации в ФНС
    #input:  JSON                    exampl: JSON Ticket
    #output: Ticket(models.Model)    exampl: Ticket(models.Model)
    #        Buy(models.Model)       exampl: Buy(models.Model)
    ################################################
    def __init__(self, JSON: str):

        self.Ticket = None
        self.Buylist = []
        self.errorlist =[]
        if str(JSON['dateTime']).isdigit():
            self.datetime = get_UNIXtoTime(JSON['dateTime'])
        else:
            self.datetime = datetime.fromisoformat(JSON['dateTime']) 
        self._get_Ticket(JSON)
        self._get_Buy(JSON)

    def _set_field(self, ModelField: str, JSONField: str , JSON: json) -> dict:
        #Функция для обработки и сопоставления поля в Model и поля в JSON
        __dict ={}
        try:
            if JSONField in JSON:
                # В JSON все суммы без . 140700 = 1407.00
                if  JSONField == 'totalSum' or JSONField == 'ecashTotalSum' or JSONField == 'cashTotalSum' \
                    or JSONField == 'price' or JSONField == 'sum' or JSONField == 'ndsSum':
                    __dict[ModelField] = str(JSON[JSONField]/100)
                else:    
                    __dict[ModelField] = str(JSON[JSONField]).strip()
            else:
                __dict[ModelField] = ''
                
                if JSONField == 'ndsSum':
                    __dict[ModelField] = 0
                
            return __dict
        except:
            self.errorlist.append(f'Ошибка парсинга JSON, в поле: {JSONField}')
            return None

    def _get_Ticket(self, JSON: str) -> object:
        #Функция для получения обьекта Ticket(models.Model)
        ticket_dict ={}
        
        #Словарь сопоставления поля в Моделе и поля в JSON
        #Ticket.Model : JSON['field']
        __field_dict = {
            'operationType' : "operationType",
            'totalsum' : "totalSum",
            'cashTotalSum' : "cashTotalSum",
            'ecashTotalSum' : "ecashTotalSum",
            'taxationType' : "taxationType",
            'internetSign' : "internetSign",
            'fd' : "fiscalDocumentNumber",
            'fn' : "fiscalDriveNumber",
            'fp' : "fiscalSign",
            'kkt' : "kktRegId",
            'requestNumber' : "requestNumber",
            'operator' : 'operator',
            'shiftnumber' : "shiftNumber",
            'retailplace' : "retailPlace",
            'user' : "user",
            'userInn': "userInn",
        }
        
        for key in __field_dict:
            __row = self._set_field(key,__field_dict[key], JSON)
            if __row != None:
                ticket_dict.update(__row)

        ticket_dict['datetime'] = self.datetime
        ticket_dict['json'] = JSON
        ticket_dict['qr'] =get_qrText(self.datetime,
                                        ticket_dict['totalsum'],
                                        ticket_dict['fn'],
                                        ticket_dict['fd'],
                                        ticket_dict['fp'],
                                        ticket_dict['operationType'])
        
        if not self.errorlist:
            self.Ticket = Ticket(**ticket_dict)

    def _get_Buy(self, JSON: str) -> object:
        #Функция для получения обьекта Buy(models.Model)
        
        #Словарь сопоставления поля в Моделе и поля в JSON
        #Ticket.Model : JSON['field']
        buy_dict = {}
        __field_dict = {
            'name' : "name",
            'nds' : "nds",
            'paymentType' : "paymentType",
            'price' : "price",
            'productType' : "productType",
            'quantity' : "quantity",
            'sum' : "sum",
            'ndsSum' : "ndsSum",
        }
        try:
            for i, value in enumerate(JSON["items"]):
                buy_dict = {}
                for key in __field_dict:
                    __row = self._set_field(key,__field_dict[key], JSON["items"][i])
                    if __row != None:
                        buy_dict.update(__row)
                if not self.errorlist:
                    self.Buylist.append(Buy(state = 'Buy', dateTime = self.datetime, **buy_dict))
        except:
            self.errorlist.append(f'Ошибка парсинга JSON(Buy)')