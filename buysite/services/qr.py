import re
from buysite.models import Ticket
from datetime import datetime

class QR:
    
    ################################################
    #Класс для проверки и работы с QR
    #input:  JSON                    exampl: JSON Ticket
    #output: Ticket(models.Model)    exampl: Ticket(models.Model)
    #        Buy(models.Model)       exampl: Buy(models.Model)
    ################################################
    def __init__(self, qr: str):
        self.qr = str(qr).strip()
        self.errorlist =[]
        self._validation()
    
    def _validation(self) -> None:
        
        #t=20211201T201800&s=1407.00&fn=9960440300678043&i=9546&fp=697923058&n=1
        
        template = '(^t=)+(\d{8})+T+(\d{6}|\d{4})+&s=+(\d{1,6}).(\d{2})+&fn=+(\d{16})+&i=+(\d{1,16})+&fp=+(.{1,30})+&n=1'
        
        if re.match(template, self.qr) is None:
            self.errorlist.append('QR код не соответствует шаблону')
        else:
            self._splitQr()
    
    def _splitQr(self) -> None:
        __list=[]
        __list = self.qr.split("&")
        
        #дата
        __date = __list[0]
        try:
            self.date = datetime.strptime(__date, "t=%Y%m%dT%H%M%S")
        except:
            self.errorlist.append('QR. Не возможно преобразовать дату('+ __date +')')
            
        #сумма
        __sum = __list[1][2:]
        try:
            self.sum = float(__sum)
        except:
            self.errorlist.append('QR. Не возможно преобразовать сумму('+ __sum +')')
            
        #ФН
        __fn = __list[2][3:]
        try:
            self.fn = int(__fn)
        except:
            self.errorlist.append('QR. Не возможно преобразовать ФН('+ __fn +')')

        if len(__fn) > 16:
            self.errorlist.append('QR. ФН должно быть не более 16 символов')
        
        #ФД
        __fd = __list[3][2:]
        try:
            self.fd = int(__fd)
        except:
            self.errorlist.append('QR. Не возможно преобразовать ФД('+ __fd +')')
        
        if len(__fd) > 16:
            self.errorlist.append('QR. ФД должно быть не более 16 символов')
        
        #ФП
        __fp = __list[4][3:]
        try:
            self.fp = int(__fp)
        except:
            self.errorlist.append('QR. Не возможно преобразовать ФП('+ __fp +')')
        
        if len(__fp) > 16:
            self.errorlist.append('QR. ФП должно быть не более 16 символов')
        
        #Признак расчета
        __type = __list[5][2:]
        try:
            self.type = int(__type)
        except:
            self.errorlist.append('QR. Не возможно преобразовать Признак расчета('+ __type +')')
        
        if len(__type) > 1:
            self.errorlist.append('QR. ФП должно быть не более 1 символа('+ __type +')')
    
    def getTiket(self) -> None:

        if self.errorlist:
            return None

        if Ticket.objects.filter(fn = self.fn, fd = self.fd, fp = self.fp):
            return Ticket.objects.get(fn = self.fn, fd = self.fd, fp = self.fp)
        else:
            return None


    def __str__(self):
        return str(self.qr)