from datetime import datetime, timedelta
from .qr import QR

def get_UNIXtoTime(time: str) -> datetime:
    #Функция для преобразования Unix времени, с учетом пояса
    
    delta = timedelta(hours=3, minutes=0)
    date = datetime.utcfromtimestamp(time).strftime('%Y-%m-%d %H:%M:%S+00:00')
    
    return datetime.fromisoformat(date) + delta

def get_qrText(dt: datetime, sum: str, fn: int, fd: int, fp: int, operationType: int) -> str:
    #Функция для формирование текстового вида qr-кода
    qrstr = ''
    qrstr += 't=' + str(dt.strftime('%Y%m%dT%H%M%S'))
    
    sum = str(sum).replace(',','.')
    if sum.rfind('.') == -1:
        sum += '.00'
    else:
        if sum.rfind('.') == 1:
            sum += '0'
    
    qrstr += '&s=' + sum
    qrstr += '&fn=' + str(fn)
    qrstr += '&i=' + str(fd)
    qrstr += '&fp=' + str(fp)
    qrstr += '&n=' + str(operationType)
    
    return qrstr