from distutils.util import execute
import json
import traceback
import openpyxl
import os.path
from django.conf import settings
from datetime import datetime, timedelta


BASIC_PATH = os.path.join(settings.MEDIA_ROOT, 'xls/')

class EXEL:
    
    ################################################
    #Класс для создания и обработки exel
    #input:  Buy(models.Model)       exampl: Buy(models.Model)
    #input:  name                    exampl: '32143254234'
    #output: url                     exampl: /media/xls/32143254234.xls
    ################################################
    def __init__(self, Buy):

        self.path = BASIC_PATH + self._get_name(Buy)
        self.errorlist =[]
        self._create_exel()
        
        if not self.errorlist:
            self._rec_exel(Buy)
        
    def _get_name(self, Buy) -> str:
        #Функция для получения имени
        
        strhash = ''
        for b in Buy:
            strhash += b.name
        
        return str(hash(strhash)) + '.xlsx'
    
    def _create_exel(self) -> None:
        try:
            if not os.path.exists(self.path):
                excel_file = openpyxl.Workbook()
                excel_file.create_sheet(title="Покупки",index=0)

                sheet = excel_file["Покупки"]
                sheet.cell(row=1, column=1).value = "Наименовение"
                sheet.cell(row=1, column=2).value = "Дата/время покупки"
                sheet.cell(row=1, column=3).value = "Цена"
                sheet.cell(row=1, column=4).value = "Колличество"
                sheet.cell(row=1, column=5).value = "Единица измерения"
                sheet.cell(row=1, column=6).value = "Сумма"
                sheet.cell(row=1, column=7).value = "Сумма НДС"
                sheet.cell(row=1, column=8).value = "Ставка НДС"
                sheet.cell(row=1, column=9).value = "Группа"
                sheet.cell(row=1, column=10).value = "Комментарий"
                excel_file.save(self.path)
                excel_file.close()
                
        except Exception as err:
            self.errorlist.append(f'Ошибка при создании файла exel:{traceback.format_exc()}')
                
    def _rec_exel(self, Buy) -> None:
        
        try:
            wb = openpyxl.load_workbook(filename = self.path)
            sheet = wb["Покупки"]
            num_row = sheet.max_row + 1
            
            for i,buy in enumerate(Buy):
                sheet.cell(row=i+num_row, column=1).value = str(buy.name)
                sheet.cell(row=i+num_row, column=2).value = str(buy.dateTime)
                sheet.cell(row=i+num_row, column=3).value = str(buy.price)
                sheet.cell(row=i+num_row, column=4).value = str(buy.quantity)
                sheet.cell(row=i+num_row, column=5).value = str(buy.measure)
                sheet.cell(row=i+num_row, column=6).value = str(buy.sum)
                sheet.cell(row=i+num_row, column=7).value = str(buy.ndsSum)
                sheet.cell(row=i+num_row, column=8).value = str(buy.nds)
                sheet.cell(row=i+num_row, column=9).value = str(buy.group)
                sheet.cell(row=i+num_row, column=10).value = str(buy.comment)
                
            #Подгоним размер колонок
            RATIO = 1.15 # Коофициент, подбирается вручную в зависимости от Шрифта
            
            max_len_column = 0
            for column in sheet.columns:
                for cell in column:
                    len_cell = len(str(cell.value))
                    if max_len_column < len_cell:
                        max_len_column = len_cell
                    column_letter = cell.column_letter

                sheet.column_dimensions[column_letter].width = max_len_column * RATIO
                max_len_column = 0

            wb.save(self.path)
            wb.close()
                
        except Exception as err:
            self.errorlist.append(f'Ошибка при заполнении файла exel:{traceback.format_exc()}')














