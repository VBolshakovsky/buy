from django.apps import AppConfig


class BuysiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'buysite'
