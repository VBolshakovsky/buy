from datetime import datetime, date, time, timedelta
import json
import traceback
import qrcode

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import FileResponse, HttpResponse
from django.urls import reverse
from django.db.models import Q
from django.shortcuts import redirect, render

from .forms import AddGroupForm, AddQRDetailForm, AddQRFileForm, AddQRLineForm, SendSMSForm
from .forms import UserRegistrationForm, UserEditForm, ProfileEditForm, ProfileRegistrationForm, AddPersonalForm, AddBuyForm
from .models import Buy, Ticket, Profile, Group, Сategory
from .services.nalog import NalogAuth
from .services.qr import QR
from .services.parser import Parser
from .services.exel import EXEL
from .services.functions import get_qrText

from django.core.paginator import Paginator
buy_page_count = 20

################################################
#Главная страница с загрузкой данных
################################################
def index(request):
    context = {}
    out = []
    qrlist = []
    error = []
    globalError = []
    SMSButton = False
    activeTab = 'details'
    
    #Подствим номер телефона если пользователь зарегистрирован
    if request.user.is_authenticated:
        user = User.objects.get(username=request.user)

    if request.method == 'POST':
        QRDetailForm = AddQRDetailForm(request.POST)
        SMSForm = SendSMSForm(request.POST)
        QRLineForm = AddQRLineForm(request.POST)
        QRFileForm = AddQRFileForm(request.POST, request.FILES)
        
        
        if SMSForm.is_valid():
            sms = SMSForm.cleaned_data.get('sms')
        else:
            sms = None
        
        if QRDetailForm.is_valid():
            #Присваиваем полученные с формы данные в переменную
            qrParam = QRDetailForm.cleaned_data

            #Сохранить фокус на tab details
            if request.POST.get('check-details') or request.POST.get('send-details'):
                
                activeTab = 'details'
                nalogAuth = NalogAuth(qrParam['tel'])

            #Получаем цельный QR текст с формы
            _datetime = datetime.combine(qrParam['date'], qrParam['time'])
            qrlist.append(get_qrText(_datetime,
                                        qrParam['sum'],
                                        qrParam['fn'],
                                        qrParam['fd'],
                                        qrParam['fp'],
                                        qrParam['type']))

        if QRLineForm.is_valid():
            qrParam = QRLineForm.cleaned_data
            
            #Сохранить фокус на tab line
            if request.POST.get('check-line') or request.POST.get('send-line'):
                activeTab = 'line'
                nalogAuth = NalogAuth(qrParam['tel'])
            
            qrParam = QRLineForm.cleaned_data
            qrlist =  str(qrParam['QRLine']).split("\n")
                    
        for qrtext in qrlist:
            #Проверим qrtext на корректность
            qr = QR(qrtext)
            if qr.errorlist:
                out.append({'qrtext': qrtext, 'errorlist': qr.errorlist, 'Ticket': None, 'Buylist': None})
            else:
                #Проверяем есть ли уже такой в базе
                if qr.getTiket() is not None and not qrParam['save']:
                    ticket = qr.getTiket()
                    buy = Buy.objects.filter(ticket = ticket)
                    out.append({'qrtext': qrtext, 'errorlist': None, 'Ticket': ticket, 'Buylist': list(buy)})
                else:
                    #Работаем с кодом СМС
                    if sms is not None:
                        SMSButton = False
                        nalogAuth.get_sessionId(sms)
                        #Пробуем получить JSON
                        JSON = nalogAuth.get_ticket(qrtext)
                        if nalogAuth.errorlist:
                            out.append({'qrtext': qrtext, 'errorlist': nalogAuth.errorlist, 'Ticket': None, 'Buylist': None})
                            nalogAuth.errorlist = []
                            globalError.append('Ошибка при авторизации')
                    else:
                        #Пробуем получить JSON
                        JSON = nalogAuth.get_ticket(qrtext)
                        
                        if nalogAuth.errorlist:
                            if nalogAuth.refresh_token is not None:
                                nalogAuth.errorlist = []
                                
                                #Обновим session_id
                                nalogAuth.refresh_session_id()
                        
                                #Пробуем получить JSON еще раз
                                JSON = nalogAuth.get_ticket(qrtext)
                            
                            if nalogAuth.errorlist or nalogAuth.refresh_token is None:
                                #Обновим session_id через смс
                                nalogAuth.errorlist = []
                                nalogAuth.get_SMScode()
                                SMSButton = True
                                
                    if not globalError and SMSButton == False:
                        
                        try:
                            print(JSON)
                            parser = Parser(JSON["ticket"]["document"]["receipt"])

                            if not parser.errorlist:
                                
                                try:
                                    #Сохранение
                                    if qrParam['save']:

                                        ticket = parser.Ticket
                                        # Если такой чек уже есть не сохраняем, и берем из базы
                                        if not Ticket.objects.filter(fn = parser.Ticket.fn, fd = parser.Ticket.fd, fp = parser.Ticket.fp):
                                            ticket.save()
                                        else:
                                            ticket = Ticket.objects.get(fn = parser.Ticket.fn, fd = parser.Ticket.fd, fp = parser.Ticket.fp)
                                            
                                        for __Buy in parser.Buylist:
                                            __Buy.ticket = ticket
                                            
                                            if request.user.is_authenticated:
                                                __Buy.user = user
                                            
                                            # Если есть такая покупка не сохраняем
                                            if not Buy.objects.filter(name = __Buy.name,
                                                                  dateTime  = __Buy.dateTime,
                                                                  price  = __Buy.price,
                                                                  quantity  = __Buy.quantity,
                                                                  sum  = __Buy.sum,
                                                                  ticket  = __Buy.ticket,
                                                                  user = user):
                                                #Найдем группу, если товар с таким названием уже есть
                                                if not __Buy.group:
                                                    buylist = Buy.objects.filter(Q(name = __Buy.name) & Q(group__isnull=False) & Q(user = user))
                                                    if buylist:
                                                        __Buy.group = buylist[0].group
                                                        
                                                __Buy.save()
                                    
                                    out.append({'qrtext': qrtext, 'errorlist': None, 'Ticket': parser.Ticket, 'Buylist': parser.Buylist})
                                except Exception as err:
                                    error.append(f'Ошибка при сохранении:{traceback.format_exc()}')
                                    out.append({'qrtext': qrtext, 'errorlist': error, 'Ticket': parser.Ticket, 'Buylist': parser.Buylist})

                            else:
                                out.append({'qrtext': qrtext, 'errorlist': parser.errorlist, 'Ticket': None, 'Buylist': None})
                        except Exception as err:
                            error.append(f'Ошибка парсера:{traceback.format_exc()}')
                            out.append({'qrtext': qrtext, 'errorlist': error, 'Ticket': None, 'Buylist': None})
                            
        if QRFileForm.is_valid():
            #Сохранить фокус на tab file
            if request.POST.get('check-file') or request.POST.get('send-file'):
                activeTab = 'file'
                
            #Проверим расщирение файла(.json)
            qrParam = QRFileForm.cleaned_data
            if qrParam['file'].name.split('.')[-1] != 'json':
                globalError.append('Не верный формат файла, должен быть .json')
            else:
                try:
                    data = request.FILES['file'].read()

                    JSONlist = json.loads(data.decode('utf-8'))
                    for JSON in JSONlist:
                        parser = Parser(JSON["ticket"]["document"]["receipt"])

                        qrtext = get_qrText(parser.Ticket.datetime,
                                                parser.Ticket.totalsum,
                                                parser.Ticket.fn,
                                                parser.Ticket.fd,
                                                parser.Ticket.fp,
                                                parser.Ticket.operationType)

                        if not parser.errorlist:
                            out.append({'qrtext': qrtext, 'errorlist': None, 'Ticket': parser.Ticket, 'Buylist': parser.Buylist})

                            #Сохранение
                            if not Ticket.objects.filter(fn = parser.Ticket.fn, fd = parser.Ticket.fd, fp = parser.Ticket.fp):
                                if qrParam['save']:
                                    ticket = parser.Ticket
                                    
                                    # Если такой чек уже есть не сохраняем, и берем из базы
                                    if not Ticket.objects.filter(fn = parser.Ticket.fn, fd = parser.Ticket.fd, fp = parser.Ticket.fp):
                                        ticket.save()
                                    else:
                                        ticket = Ticket.objects.get(fn = parser.Ticket.fn, fd = parser.Ticket.fd, fp = parser.Ticket.fp)
                                    
                                    for __Buy in parser.Buylist:
                                        __Buy.ticket = ticket
                                        
                                        if request.user.is_authenticated:
                                            __Buy.user = user
                                        
                                        # Если есть такая покупка не сохраняем
                                        if not Buy.objects.filter(name = __Buy.name,
                                                              dateTime  = __Buy.dateTime,
                                                              price  = __Buy.price,
                                                              quantity  = __Buy.quantity,
                                                              sum  = __Buy.sum,
                                                              ticket  = __Buy.ticket,
                                                              user = user):
                                            #Найдем группу, если товар с таким названием уже есть
                                            if not __Buy.group:
                                                buylist = Buy.objects.filter(Q(name = __Buy.name) & Q(group__isnull=False) & Q(user = user))
                                                if buylist:
                                                    __Buy.group = buylist[0].group
                                            
                                            __Buy.save()
                                        
                        else:
                            out.append({'qrtext': qrtext, 'errorlist': parser.errorlist, 'Ticket': None, 'Buylist': None})
                except Exception as err:
                    globalError.append(f'Ошибка при обработке файла:{traceback.format_exc()}')
                    
    else:
        
        #Подставим номер телефона если пользователь зарегистрирован
        if request.user.is_authenticated:
            QRDetailForm = AddQRDetailForm(initial={'tel': user.profile.tel})
            QRLineForm = AddQRLineForm(initial={'tel': user.profile.tel})
            QRFileForm = AddQRFileForm(initial={'tel': user.profile.tel})
        else:
            QRDetailForm = AddQRDetailForm()
            QRLineForm = AddQRLineForm()
            QRFileForm = AddQRFileForm()
        
        SMSForm = SendSMSForm()
    
    context['QRFileForm'] = QRFileForm
    context['QRLineForm'] = QRLineForm
    context['QRDetailForm'] = QRDetailForm
    context['SMSForm'] = SMSForm
    context['out'] = out
    context['SMSButton'] = SMSButton
    context['activeTab'] = activeTab
    context['globalError'] = globalError
    
    return render(request,'buysite/index.html', context)

################################################
#Отображение покупок(личный кабинет)
################################################

@login_required
def personal(request):
    context = {}
    buylist = []
    alert =[]
    
    dateFrom = datetime.date(datetime.now()-timedelta(days=3))
    dateBy = datetime.date(datetime.now())
    
    buylistNoGroup = Buy.objects.filter(user = request.user, group__isnull=True).order_by('-dateTime')
    if request.path == reverse('nogroup'):
        if buylistNoGroup:
            request.session['dateFrom'] = str(datetime.date(buylistNoGroup[0].dateTime))
            request.session['dateBy'] = str(datetime.date(datetime.now()))
    
    alert.append({'text':f'{len(buylistNoGroup)} товаров с не определенной группой', 'URL': reverse('nogroup')})
    
    #Удаление
    if request.POST.get('del'):
        buydel = Buy.objects.get(id = request.POST.get('del'))
        buydel.delete()
        
    #Редактировать
    if request.POST.get('edit'):
        request.session['id'] = request.POST.get('edit')
        return redirect('editbuy')

    #Поиск
    searsh_query = request.POST.get('search', '')

    if request.method == 'POST':
        personalForm = AddPersonalForm(request.POST)
        if personalForm.is_valid():
            request.session['dateFrom'] = str(personalForm.cleaned_data.get('dateFrom'))
            request.session['dateBy'] = str(personalForm.cleaned_data.get('dateBy'))
            
            if request.POST.get('three-days'):
                request.session['dateFrom'] = str(datetime.date(datetime.now()-timedelta(days=3)))
                request.session['dateBy'] = str(datetime.date(datetime.now()))
            
            if request.POST.get('week'):
                request.session['dateFrom'] = str(datetime.date(datetime.now()-timedelta(days=7)))
                request.session['dateBy'] = str(datetime.date(datetime.now()))
        
            if request.POST.get('month'):
                request.session['dateFrom'] = str(datetime.date(datetime.now()-timedelta(days=30)))
                request.session['dateBy'] = str(datetime.date(datetime.now()))
            
            if request.POST.get('all'):
                buylistd = Buy.objects.filter(user = request.user).order_by('dateTime')
                if buylistd:
                    request.session['dateFrom'] = str(datetime.date(buylistd[0].dateTime))
                    request.session['dateBy'] = str(datetime.date(datetime.now()))

    if request.session.get('dateFrom'):
        dateFrom = datetime.date(datetime.fromisoformat(request.session.get('dateFrom')))
    if request.session.get('dateBy'):
        dateBy = datetime.date(datetime.fromisoformat(request.session.get('dateBy')))

    if searsh_query:
        buylist = Buy.objects.filter(Q(user = request.user) & Q(name__icontains=searsh_query)).order_by('dateTime')
        if buylist:
            request.session['dateFrom'] = str(datetime.date(buylist[0].dateTime))
            dateFrom = str(datetime.date(buylist[0].dateTime))
    else:
        buylist = Buy.objects.filter(user = request.user, dateTime__gte=dateFrom, dateTime__lte=dateBy).order_by('-dateTime')

    if request.path == reverse('nogroup'):
        if searsh_query:
            buylist = Buy.objects.filter(Q(user = request.user) & Q(name__icontains=searsh_query) & Q(group__isnull=True)).order_by('dateTime')
            if buylist:
                request.session['dateFrom'] = str(datetime.date(buylist[0].dateTime))
                dateFrom = str(datetime.date(buylist[0].dateTime))
        else:
            buylist = Buy.objects.filter(user = request.user, group__isnull=True, dateTime__gte=dateFrom, dateTime__lte=dateBy).order_by('-dateTime')
    
    #Отдадим файл если попросят
    if buylist and request.POST.get('exel'):
        exel = EXEL(buylist)
        
        if not exel.errorlist:
            response = FileResponse(open(exel.path, 'rb'))
            return response

    personalForm = AddPersonalForm(initial={'dateFrom': dateFrom, 'dateBy': dateBy})
    
    #Пагинация
    paginator = Paginator(buylist, buy_page_count)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    if page_obj.has_next():
        next_url = f'?page={page_obj.next_page_number()}'
    else:
        next_url = ''
    if page_obj.has_previous():
        prev_url = f'?page={page_obj.previous_page_number()}'
    else:
        prev_url=''
     
    context = { 'page_obj': page_obj,
    'next_page_url': next_url,
    'prev_page_url': prev_url
    }
    
    context['personalForm'] = personalForm
    context['alert'] = alert
    return render(request, 'buysite/personal.html', context)

################################################
#Отображение чека
################################################
def ticket(request, fp):

    context = {}
    
    if len(Ticket.objects.filter(fp = fp)) == 1:
        ticket = Ticket.objects.get(fp = fp)
        buy = Buy.objects.filter(ticket = ticket)
        
        img = qrcode.make(ticket.qr)
        img.save("media/qr/" + str(ticket.fp) + ".png")
    else:
        return HttpResponse("Чек не найден", status=400, reason="Incorrect data")
    
    context['ticket'] = ticket
    context['buy'] = buy
    context['img'] = "/media/qr/" + str(ticket.fp) + ".png"
    
    return render(request, 'buysite/ticket.html', context)

################################################
#Отображение группы
################################################
@login_required
def group(request, group_slug):
    context = {}
    
    group = Group.objects.get(slug=group_slug)
    namelink = Buy.objects.filter(user = request.user, group = group).values('name').distinct()
    buylist = Buy.objects.filter(user = request.user, group = group).order_by('dateTime')
    
    #Удаление
    if request.POST.get('delgroup'):
        #group.delete()
        return redirect('personal')
    
    #Редактировать
    if request.POST.get('editgroup'):        
        request.session['id_group'] = group.pk
        return redirect('editgroup')
    
    #Удаление
    if request.POST.get('del'):
        buydel = Buy.objects.get(id = request.POST.get('del'))
        buydel.delete()
        
    #Редактировать
    if request.POST.get('edit'):
        request.session['id'] = request.POST.get('edit')
        return redirect('editbuy')
    
    
    #Отдадим файл если попросят
    if buylist and request.POST.get('exel'):
        exel = EXEL(buylist)
        
        if not exel.errorlist:
            response = FileResponse(open(exel.path, 'rb'))
            return response
    
    #Пагинация
    paginator = Paginator(buylist, buy_page_count)
     
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    if page_obj.has_next():
        next_url = f'?page={page_obj.next_page_number()}'
    else:
        next_url = ''
    if page_obj.has_previous():
        prev_url = f'?page={page_obj.previous_page_number()}'
    else:
        prev_url=''
     
    context = { 'page_obj': page_obj,
    'next_page_url': next_url,
    'prev_page_url': prev_url
    }
    
    
    context['group'] = group
    context['namelink'] = namelink
    context['buylist'] = buylist
    
    return render(request, 'buysite/group.html', context)

################################################
#Добавление группы
################################################
@login_required
def addGroup(request):
    context = {}
    
    if request.method == 'POST':
        GroupForm = AddGroupForm(request.POST)

        if GroupForm.is_valid():
            group = GroupForm.save(commit=False)
            group.save()

            return redirect('personal')
    else:
        GroupForm = AddGroupForm()
    
    context['GroupForm'] = GroupForm
    
    return render(request, 'buysite/addgroup.html', context)

################################################
#Редактирование группы
################################################
@login_required
def editGroup(request):
    context = {}
    
    if request.method == 'POST':
        GroupForm = AddGroupForm(request.POST)
        if GroupForm.is_valid():
            param = GroupForm.cleaned_data
            # Обновим обьект, перед его сохранением
            group = Group.objects.get(pk = request.session['id_group'])
            group.name = param['name']
            group.category = param['category']
            group.measure = param['measure']
            group.comment = param['comment']
            
            #group.save()

            return redirect('personal')
    else:
        group = Group.objects.get(pk = request.session['id_group'])
        GroupForm = AddGroupForm(initial={'name': group.name,
                    'category': group.category,
                    'measure': group.measure,
                    'comment': group.comment,
                      })

    context['GroupForm'] = GroupForm
    
    return render(request, 'buysite/editgroup.html', context)

################################################
#Редактирование покупки
################################################
@login_required
def editbuy(request):
    context = {}
    user = User.objects.get(username=request.user)
    
    if request.method == 'POST':
        BuyForm = AddBuyForm(request.POST)
        if BuyForm.is_valid():
            param = BuyForm.cleaned_data
            # Обновим обьект, перед его сохранением
            buy = Buy.objects.get(pk = request.session['id'])
            
            buy.name = param['name']
            buy.state = param['state']
            buy.dateTime = param['dateTime']
            buy.price = param['price']
            buy.quantity = param['quantity']
            buy.measure = param['measure']
            buy.group = param['group']
            buy.comment = param['comment']            
            buy.sum = buy.price * buy.quantity
            
            buy.save()
            
            #Если была выбрана группа, обновим для всех с таким же именем, у кого не было
            if buy.group:
                buylist = Buy.objects.filter(Q(name = buy.name) & Q(group__isnull=True)& Q(user=user)).update(group=buy.group)
                
            return redirect('personal')
    else:
        buy = Buy.objects.get(pk = request.session['id'])
        BuyForm = AddBuyForm(initial={'name': buy.name,
                    'state': buy.state,
                    'dateTime': buy.dateTime,
                    'price': buy.price,
                    'quantity': buy.quantity,
                    'measure': buy.measure,
                    'group': buy.group,
                    'comment': buy.comment,
                      })

    context['BuyForm'] = BuyForm
    
    return render(request, 'buysite/editbuy.html', context)

################################################
#Добавление покупки
################################################
@login_required
def addbuy(request):
    context = {}
    
    user = User.objects.get(username=request.user)
    
    if request.method == 'POST':
        BuyForm = AddBuyForm(request.POST)
        if BuyForm.is_valid():
            # Дополним обьект, перед его сохранением
            buy = BuyForm.save(commit=False)
            buy.user = user
            buy.sum = buy.price * buy.quantity
            
            #Найдем группу, если товар с таким названием уже есть
            if not buy.group:
                buylist = Buy.objects.filter(Q(name = buy.name) & Q(group__isnull=False) & Q(user = user))
                if buylist:
                    buy.group = buylist[0].group
            buy.save()
                        
            # Для всех уже сохраненных с таким же названием, но без группы - обновим группу
            if buy.group:
                buylist = Buy.objects.filter(Q(name = buy.name) & Q(group__isnull=True)& Q(user=user)).update(group=buy.group)
            return redirect('personal')
    else:
        BuyForm = AddBuyForm()
    
    context['BuyForm'] = BuyForm
    
    return render(request, 'buysite/addbuy.html', context)

################################################
#Отображение категорий
################################################

@login_required
def category(request, category_slug):
    context = {}
    
    сat = Сategory.objects.get(slug=category_slug)
    namelink = Buy.objects.filter(user = request.user, group__category = сat).values('name').distinct()
    buylist = Buy.objects.filter(user = request.user, group__category = сat).order_by('dateTime')
    
    #Удаление
    if request.POST.get('del'):
        buydel = Buy.objects.get(id = request.POST.get('del'))
        buydel.delete()
        
    #Редактировать
    if request.POST.get('edit'):
        request.session['id'] = request.POST.get('edit')
        return redirect('editbuy')
    
    #Отдадим файл если попросят
    if buylist and request.POST.get('exel'):
        exel = EXEL(buylist)
        
        if not exel.errorlist:
            response = FileResponse(open(exel.path, 'rb'))
            return response
    
    #Пагинация
    paginator = Paginator(buylist, buy_page_count)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    if page_obj.has_next():
        next_url = f'?page={page_obj.next_page_number()}'
    else:
        next_url = ''
    if page_obj.has_previous():
        prev_url = f'?page={page_obj.previous_page_number()}'
    else:
        prev_url=''
     
    context = { 'page_obj': page_obj,
    'next_page_url': next_url,
    'prev_page_url': prev_url
    }
    
    context['category'] = сat
    context['namelink'] = namelink
    context['buylist'] = buylist
    
    return render(request, 'buysite/category.html', context)

################################################
#Регистрация пользователей
################################################
def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        prof_form = ProfileRegistrationForm(request.POST)
        if user_form.is_valid() and prof_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            
            Profile.objects.create(user=new_user, tel = prof_form.cleaned_data['tel'])

            return render(request, 'buysite/account/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
        prof_form = ProfileRegistrationForm()
        
    return render(request, 'buysite/account/register.html', {'user_form': user_form, 'prof_form': prof_form})

################################################
#Редактирование пользователя
################################################
@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile, data=request.POST, files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
        return render(request,
                      'buysite/account/register_done.html')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
        return render(request,
                      'buysite/account/edit.html',
                      {'user_form': user_form,
                       'profile_form': profile_form})