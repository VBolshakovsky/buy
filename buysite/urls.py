from django.urls import path, re_path
from buysite import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordChangeDoneView

urlpatterns = [
    path('register/', views.register, name='register'),
    path('edit/', views.edit, name='edit'),
    path('login/', LoginView.as_view(template_name='buysite/account/login.html'), name='login'),
    path('logout/',LogoutView.as_view(template_name='buysite/account/logout.html'),name = 'logout'),
    path('password-change/', PasswordChangeView.as_view(template_name='buysite/account/password_change_form.html'), name='password_change'),
    path('password-change/done/', PasswordChangeDoneView.as_view(template_name='buysite/account/password_change_done.html'), name='password_change_done'),
    
    path('', views.index, name='home'),
    path('ticket/<int:fp>', views.ticket, name='ticket'),
    path('personal/', views.personal, name='personal'),
    path('personal/nogroup/', views.personal, name='nogroup'),
    
    
    path('addbuy/', views.addbuy, name='addbuy'),
    path('editbuy/', views.editbuy, name='editbuy'),
    
    path('group/add/', views.addGroup, name='addgroup'),
    path('group/edit/', views.editGroup, name='editgroup'),
    path('group/<slug:group_slug>/', views.group, name='group'),
    
    path('category/<slug:category_slug>/', views.category, name='category'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)